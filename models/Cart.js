const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    items: [
        {
            _id: false,
            productId: {
                type: mongoose.Types.ObjectId,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            quantity: {
                type: Number,
                default: 1
            },
            price : {
                type : Number,
                required : true
            },
            image : {
                type : String,
                required:true
            },
            subtotal: {
                type: Number
            }
        }
    ],
    total: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("Cart", cartSchema);
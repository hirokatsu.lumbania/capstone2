const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required!"]
    },
    description: {
        type: String,
        required: [true, "Product description is required!"]
    },
    price: {
        type: Number,
        required: [true, "Please include a price!"]
    },
    category : {
        type: String,
        required : [true, "Please include a category!"]
    },
    image : {
        type : String,
        required : [true, "Please include image link!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Product", productSchema);
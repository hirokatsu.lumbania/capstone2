const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        required: [true, "Who's order is this?"]
    },
    products: [
        {
            _id: false,
            productId: {
                type: mongoose.Types.ObjectId,
                required: [true, "What is the product ID?"],
                ref: 'product'
            },
            name: {
                type: String,
                required: true
            },
            quantity: {
                type: Number,
                default: 1
            },
            price : {
                type : Number,
                required : true
            },
            image : {
                type : String,
                required:true
            },
            subtotal: {
                type: Number
            }
        }
    ],
    totalAmount: {
        type: Number
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);
const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Cart = require("../models/Cart.js");

module.exports.register = (request, response) => {
    let UserPassword;
    if (request.body.password == "" || request.body.email == "") {
        return response.status(400).send({
            message: "Please enter both email and password"
        })
    } else {
        UserPassword = bcrypt.hashSync(request.body.password, 10);
    }

    User.findOne({ email: request.body.email })
        .then(result => {
            if (result != null) {
                return response.status(409).send({
                    message: "Email address already in use!"
                })
            }

            let newUser = new User({
                firstName : request.body.firstName,
                lastName : request.body.lastName,
                mobileNo : request.body.mobileNo,
                email: request.body.email,
                password: UserPassword,
                address : request.body.address
            })

            newUser.save().then((user) => {

                let newCart = new Cart({
                    userId: user._id
                });

                newCart.save().then(cart => {
                    return response.status(201).send({
                        message: `Successfully registered user ${user.firstName} ${user.lastName}`
                    })
                }).catch(error => response.status(500).send({ message: error.message }))
            }).catch(error => response.status(500).send({ message: error.message }))

        })
}

module.exports.login = (request, response) => {
    User.findOne({ email: request.body.email })
        .then(result => {
            if (result == null) {
                return response.status(400).send({
                    message: "User not registed. Please register for an account."
                })
            }

            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if (!isPasswordCorrect) {
                return response.status(400).send({
                    message: "Email or Password is incorrect!"
                })
            }
            return response.send({
                message: `Welcome, ${result.email}`,
                token: auth.createToken(result)
            })
        })
}

module.exports.userDetails = (request, response) => {
    User.findById(request.user.id, {
        password: 0
    }).then(result => {
        response.send(result);
    }).catch(error => response.status(500).send({ message: error.message }))
}

module.exports.setToAdmin = (request, response) => {
    User.findOneAndUpdate({
        $or: [
            { _id: request.body._id },
            { email: request.body.email }
        ]
    }, {
        isAdmin: true
    }).then(result => {
        Cart.findOneAndDelete({ userId: result._id }).catch(error => response.status(500).send({
            message: error.message
        }))
        response.send({
            message: `User ${result.email} updated to admin!`
        })
    }).catch(error => response.status(500).send({ message: "Please enter a valid _id or email." }))
}


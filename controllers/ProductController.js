const Product = require("../models/Product.js");
const mongoose = require("mongoose");

module.exports.addProduct = (request, response) => {
    Product.findOne({ name: request.body.name })
        .then(result => {
            if (result != null) {
                return response.status(409).send({
                    message: "Product already exists!"
                })
            }

            const newProduct = new Product({
                name: request.body.name,
                description: request.body.description,
                price: request.body.price,
                category : request.body.category,
                image : request.body.image
            })

            newProduct.save().then(addedProduct => {
                return response.status(201).send({
                    message: `Product added successfully! Product ID: ${addedProduct._id}`
                })
            }).catch(error => {
                response.status(500).send({
                    message: error.message
                })
            })
        }).catch(error => {
            response.status(500).send({
                message: error.message
            })
        })
}

module.exports.allProducts = (request, response) => {
    Product.find({}).then(result => {
        if (result.length == 0) {
            return response.send({
                message: "No products available"
            })
        }
        response.send(result);
    }).catch(error => response.status(500).send({
        message: error.message
    }))
}

module.exports.allActiveProducts = (request, response) => {
    Product.find({ isActive: true }).then(result => {
        if (result.length == 0) {
            return response.send({
                message: "No products available"
            })
        }
        response.send(result);
    }).catch(error => response.status(500).send({
        message: error.message
    }))
}

module.exports.getSpecificProduct = (request, response) => {
    Product.findById(
        request.params.data
    ).then(result => {
        if (result == null) {
            return response.send({
                message: "Product is not available"
            })
        }
        response.send(result);
    }).catch(error => response.status(500).send({
        message: error.message
    }))
}

module.exports.updateProduct = async (request, response) => {
    let updateDetails = {
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        category : request.body.category,
        image : request.body.image
    }

    Product.findByIdAndUpdate(request.params.data, updateDetails).then(result => {

        if (result == null) {
            return response.send({
                message: "Product does not exist"
            })
        }

        return response.send({
            message: "Product details updated successfully!"
        })
    }).catch(error => response.status(500).send({ message: error.message }))
}


module.exports.archiveProduct = (request, response) => {
    let updateDetails = {
        isActive: false
    }
    Product.findByIdAndUpdate(request.params.data, updateDetails).then(result => {

        if (result == null) {
            return response.send({
                message: "Product does not exist"
            })
        }

        return response.send({
            message: "Product archived successfully!"
        })
    }).catch(error => response.status(500).send({ message: error.message }))
}

module.exports.activateProduct = (request, response) => {
    
    let updateDetails = {
        isActive: true
    }
    Product.findByIdAndUpdate(request.params.data, updateDetails).then(result => {

        if (result == null) {
            return response.send({
                message: "Product does not exist"
            })
        }

        return response.send({
            message: "Product activated successfully!"
        })
    }).catch(error => response.status(500).send({ message: error.message }))
}


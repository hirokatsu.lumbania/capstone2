const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const Cart = require("../models/Cart.js");

module.exports.checkout = async (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    let cartHasItems;
    if (request.body.length == undefined) {
        cartHasItems = await Cart.findOne({ userId: request.user.id }).then(result => {
            if (result.items.length == 0) {
                return false
            }
            request.body = result.items;
            result.total = 0;
            result.items = [];
            result.save().catch(error => console.log(error.message))
            return true
        }).catch(error => response.status(500).send({
            message: error.message
        }))
    }

    if (cartHasItems === false) {
        return response.status(400).send({
            message: "Your cart does not have any items."
        });
    }
    let total = 0;
    let invalidProducts = 0;
    let validProducts = 0;
    let newOrder = new Order({
        userId: request.user.id
    })
    for (let index = 0; index < request.body.length; index++) {
        await Product.findOne({
            $or: [
                { name: request.body[index].name },
                { _id: request.body[index]._id }
            ]
        }).then(result => {

            if (result == null) {
                invalidProducts++;
                return;
            }
            if (result.isActive == false) {
                invalidProducts++;
                return;
            }
            newOrder.products.push({
                productId: result._id,
                name : result.name,
                quantity: request.body[index].quantity,
                price : result.price,
                image : result.image,
                subtotal : request.body[index].subtotal
            })
            let subtotal = result.price * newOrder.products[validProducts].quantity
            total += subtotal;
            validProducts++

        })
    }
    newOrder.totalAmount = total;

    if (request.body.length == invalidProducts) {
        return response.status(400).send({
            message: "There are no valid products for checkout."
        })
    }

    newOrder.save().then(result => {
        response.status(201).send(result);
    }).catch(error => response.status(500).send({
        message: error.message
    }))
}

module.exports.getOrders = (request, response) => {
    Order.find({ userId: request.user.id }).then(result => {
        if (result.length == 0) {
            return response.status(400).send({ message: "You have no existing order in our records." })
        }

        return response.send(result);
    }).catch(error => response.status(500).send({ message: error.message }));
}

module.exports.getAllOrders = (request, response) => {
    Order.find({}).then(result => {
        if (result.length == 0) {
            return response.status(400).send({
                message: "No orders exist"
            })
        }
        return response.send(result);
    }).catch(error => response.status(500).send({ message: error.message }));
}
const Cart = require("../models/Cart.js");
const Product = require("../models/Product.js");

module.exports.addItem = async (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    let product;
    let productExist = await Product.findOne({
        $or: [
            { _id: request.body._id },
            { name: request.body.name }
        ]
    }).then(result => {
        if (result == null || result.isActive == false) {
            return false;
        }
        product = result;
        return true;
    }).catch(error => response.status(500).send({
        message: error.message
    }));

    if (productExist == false) {
        return response.status(400).send({ message: "Product is not available" })
    }

    Cart.findOne({ userId: request.user.id }).then(result => {

        if (result.items.findIndex(cartItem => cartItem.name == product.name) != -1) {
            return response.status(400).send({ message: "Cart already contains this item!" })
        }

        result.items.push({
            productId: product._id,
            name: product.name,
            quantity: request.body.quantity,
            price: product.price,
            image: product.image,
            subtotal: request.body.quantity * product.price
        })

        result.total = 0;
        for (let index = 0; index < result.items.length; index++) {
            result.total += result.items[index].subtotal;
        }
        return result.save().then(updatedCart => {
            response.send({
                message: "Item added to cart!"
            })
        }).catch(error => error.message);

    }).catch(error => response.status(500).send({
        message: error.message
    }))

}

module.exports.changeQuantity = async (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    let product;
    let productExist = await Product.findOne({
        $or: [
            { _id: request.body._id },
            { name: request.body.name }
        ]
    }).then(result => {
        if (result == null) {
            return false;
        }
        product = result;
        return true;
    }).catch(error => response.status(500).send({
        message: error.message
    }));

    if (productExist == false) {
        return response.status(400).send({
            message: "Product does not exist!"
        })
    }

    await Cart.findOne({ userId: request.user.id }).then(result => {
        let index = result.items.findIndex(cartItem => cartItem.name == product.name);
        if (index == -1) {
            return response.status(400).send({
                message: "Item not in cart!"
            })
        }
        if (request.body.quantity == null) {
            return response.status(400).send({
                message: "Please enter desired quantity"
            })
        }
        result.items[index].quantity = request.body.quantity;
        result.items[index].subtotal = request.body.quantity * product.price;

        result.total = 0;
        for (let index = 0; index < result.items.length; index++) {
            result.total += result.items[index].subtotal;
        }
        return result.save().then(updatedCart => {
            response.send({
                message: `Quantity for ${product.name} updated!`
            })
        })
    }).catch(error => response.status(500).send({ message: error.messsage }))
}

module.exports.removeItem = async (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    let product;
    let productExist = await Product.findOne({
        $or: [
            { _id: request.body._id },
            { name: request.body.name }
        ]
    }).then(result => {
        if (result == null) {
            return false;
        }
        product = result;
        return true;
    }).catch(error => response.status(500).send({
        message: error.message
    }));

    if (productExist == false) {
        return response.status(400).send({
            message: "Product does not exist!"
        })
    }

    await Cart.findOne({ userId: request.user.id }).then(result => {
        let index = result.items.findIndex(cartItem => cartItem.name == product.name);
        if (index == -1) {
            return response.status(400).send({
                message: "Item is not in cart!"
            })
        }

        result.items.splice(index, 1);

        result.total = 0;
        for (let index = 0; index < result.items.length; index++) {
            result.total += result.items[index].subtotal;
        }

        return result.save().then(updatedCart => {
            return response.send({
                message: `Item ${product.name} successfully removed from cart!`
            })
        })
    }).catch(error => response.status(500).send({
        message: error.message
    }))
}

module.exports.getSubtotal = (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    Cart.findOne({ userId: request.user.id }, {
        items: 1,
        _id: 0
    }).then(result => {
        if (result.items.length == 0) {
            return response.send({
                message: "No items in cart"
            })
        }
        response.send(result);
    }).catch(error => response.status(500).send({
        message: error.message
    }));
}

module.exports.getTotal = (request, response) => {
    if (request.user.isAdmin == true) {
        return response.send({
            message: "Unable to shop with Admin Account. Please use your personal Account"
        })
    }
    Cart.findOne({ userId: request.user.id }).then(result => {
        if (result.items.length == 0) {
            return response.send({
                message: "No items in cart"
            })
        }
        return response.send({
            total: result.total
        })
    }).catch(error => response.status(500).send({
        message: error.message
    }));
}

module.exports.addOneQuantity = async (request, response) => {
    Cart.findOne({ userId: request.user.id })
    .then(result => {
        if (result === null) {
            return response.status(400).send({
                message : "Cart does not exist"
            })
        } else {
            let index = result.items.findIndex(cartItem => cartItem.productId == request.body._id);
            if (index == -1) {
                return response.status(400).send({
                    message: "Item not in cart!"
                })
            }
            result.items[index].quantity += 1;
            result.items[index].subtotal = result.items[index].price * result.items[index].quantity;
    
            result.total = 0;
            for (let index = 0; index < result.items.length; index++) {
                result.total += result.items[index].subtotal;
            }
            return result.save().then(updatedCart => {
                response.send({
                    message: `Quantity updated!`
                })
            })
        }
       
    }).catch(error => response.status(500).send({ message: error.messsage }))
}

module.exports.subtractOneQuantity = async (request, response) => {
    Cart.findOne({ userId: request.user.id })
    .then(result => {
        if (result === null) {
            return response.status(400).send({
                message : "Cart does not exist"
            })
        } else {
            let index = result.items.findIndex(cartItem => cartItem.productId == request.body._id);
            if (index == -1) {
                return response.status(400).send({
                    message: "Item not in cart!"
                })
            }
            if (result.items[index].quantity === 1) {
                return response.status(400).send({
                    message : "Cannot subtract further!"
                })
            }
            result.items[index].quantity -= 1;
            result.items[index].subtotal = result.items[index].price * result.items[index].quantity;
    
            result.total = 0;
            for (let index = 0; index < result.items.length; index++) {
                result.total += result.items[index].subtotal;
            }
            return result.save().then(updatedCart => {
                response.send({
                    message: `Quantity updated!`
                })
            })
        }
       
    }).catch(error => response.status(500).send({ message: error.messsage }))
}
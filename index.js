const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require('dotenv').config()

const port = process.env.PORT || 4000;
const app = express();
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use("/user", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);
app.use("/cart", cartRoutes);



mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-lumbania.f60xcxt.mongodb.net/E-Commerce-API?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", () => console.log("Connection Error"));
db.once("open", () => console.log("Connected to database!"));

app.listen(port, () => console.log(`Server is open at localhost:${port}`));

module.exports = app;
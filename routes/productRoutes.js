const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../auth.js")

const ProductController = require("../controllers/ProductController.js");

router.post("/admin/add-product", verify, verifyAdmin, (request, response) => {
    ProductController.addProduct(request, response);
})

router.get("/all-products", (request, response) => {
    ProductController.allProducts(request, response);
})

router.get("/all-products/active", (request, response) => {
    ProductController.allActiveProducts(request, response);
})

router.get("/:data", (request, response) => {
    ProductController.getSpecificProduct(request, response);
})

router.put("/admin/update-product/:data", verify, verifyAdmin, (request, response) => {
    ProductController.updateProduct(request, response);
})

router.put("/admin/archive/:data", verify, verifyAdmin, (request, response) => {
    ProductController.archiveProduct(request, response);
})

router.put("/admin/activate/:data", verify, verifyAdmin, (request, response) => {
    ProductController.activateProduct(request, response);
})

module.exports = router;
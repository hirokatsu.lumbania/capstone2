const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../auth.js")

const UserController = require("../controllers/UserController.js")

router.post("/register", (request, response) => {
    UserController.register(request, response);
})

router.post("/login", (request, response) => {
    UserController.login(request, response);
})

router.get("/details", verify, (request, response) => {
    UserController.userDetails(request, response);
})

router.put("/admin/convert", verify, verifyAdmin, (request, response) => {
    UserController.setToAdmin(request, response);
})



module.exports = router;
const express = require("express");
const router = express.Router();
const CartController = require("../controllers/CartController.js");
const { verify } = require("../auth.js");

router.put("/add-item", verify, (request, response) => {
    CartController.addItem(request, response);
})

router.put("/quantity", verify, (request, response) => {
    CartController.changeQuantity(request, response);
})

router.delete("/remove-item", verify, (request, response) => {
    CartController.removeItem(request, response);
})

router.get("/subtotal", verify, (request, response) => {
    CartController.getSubtotal(request, response);
})

router.get("/total", verify, (request, response) => {
    CartController.getTotal(request, response);
})

router.put("/add-one", verify, (request, response) => {
    CartController.addOneQuantity(request, response);
})

router.put("/subtract-one", verify, (request, response) => {
    CartController.subtractOneQuantity(request, response);
})

module.exports = router;
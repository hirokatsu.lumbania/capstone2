const express = require("express");
const router = express.Router();
const { verify, verifyAdmin } = require("../auth.js");
const OrderController = require("../controllers/OrderController.js");

router.post("/checkout", verify, (request, response) => {
    OrderController.checkout(request, response);
})

router.get("/my-orders", verify, (request, response) => {
    OrderController.getOrders(request, response);
})

router.get("/admin/all-orders", verify, verifyAdmin, (request, response) => {
    OrderController.getAllOrders(request, response);
})
module.exports = router;
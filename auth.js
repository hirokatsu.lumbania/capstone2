const jwt = require("jsonwebtoken");
const secretKey = "capstone2-API"

module.exports.createToken = (user) => {
    const userData = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(userData, secretKey, {});
}

module.exports.verify = (request, response, next) => {
    let token = request.headers.authorization;
    if (typeof token === "undefined") {
        return response.status(200).send({
            message: "Please login first before proceeding."
        })
    }

    token = token.slice(7, token.length);

    jwt.verify(token, secretKey, (error, decodedToken) => {
        if (error) {
            return response.status(401).send({
                message: `Authentication failed. ${error.message}`
            })
        }

        request.user = decodedToken;
        next();
    })
}

module.exports.verifyAdmin = (request, response, next) => {
    if (request.user.isAdmin) {
        return next();
    }

    return response.status(403).send({
        message: "Admin Access is required for this action."
    })
}